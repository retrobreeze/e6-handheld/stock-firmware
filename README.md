![E6 Handheld](https://i.imgur.com/t9Az27u.png)

# GAMENT E6 Handheld Stock Rom Images

This is a repository of Android 8 system images ripped from the GAMENT E6 Handheld, an RK3326-based handheld gaming device. The images were dumped using RKDumper. **Please note** that the included images do NOT include the stock game launcher, simply because I had deleted it before dumping. I will update the images to be total stock shortly.

Please fork this repository or do whatever you want with it in order to develop content for the E6!

# How to use

**I haven't tried flashing these myself**, but _in theory_, you should be able to flash them using fastboot. To enter fastboot, connect the E6 to your PC **and** to a power source (yes, it won't work without both), then run the `fastboot flash` command.

For example the recovery.img:

```
fastboot flash recovery recovery.img
```

# Auto-flasher

Coming soon!

# WARNING

I'm not responsible for any damage or bricking of your device by using these images. You have been warned!
